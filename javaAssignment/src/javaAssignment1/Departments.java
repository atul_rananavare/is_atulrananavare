package javaAssignment1;

abstract class Departments {
	private String DepartName;

	public String getDepartName() {
		return DepartName;
	}

	public void setDepartName(String departName) {
		DepartName = departName;
	}
	
	public Departments(String departments) {
		
	    this.DepartName = departments;
 
}
}

class Kitchen extends Departments {
	public Kitchen(String departments) {
		super(departments);
		// TODO Auto-generated constructor stub
	}

	void cooking() {
		System.out.println("This is Kitchen Department");
	}
	
}

class Bar extends Departments {
	public Bar(String departments) {
		super(departments);
		// TODO Auto-generated constructor stub
	}

	void bar() {
		System.out.println("This is Bar Department");
	}
}

class Dinning extends Departments {
	public Dinning(String departments) {
		super(departments);
		// TODO Auto-generated constructor stub
	}

	void dinning() {
		System.out.println("This is Dining Department");
	}
}

class Reception extends Departments {
	public Reception(String departments) {
		super(departments);
		// TODO Auto-generated constructor stub
	}

	void reception() {
		System.out.println("This is Reception Department");
	}
}

class security extends Departments {
	public security(String departments) {
		super(departments);
		// TODO Auto-generated constructor stub
	}

	void security() {
		System.out.println("This is Security Department");
	}
}
