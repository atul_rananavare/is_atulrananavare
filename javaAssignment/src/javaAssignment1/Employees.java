package javaAssignment1;

public interface Employees {

	String getEmpDesignation();

	String getEmpName();

	int getEmpId();

	String getEmpDepartment();
	
}
